function createNewUser() {
    let newUser = {};
  
    newUser.firstName = prompt("Введіть ім'я:");
    while (!/^[a-zA-Z]*$/.test(newUser.firstName)) {
      newUser.firstName = prompt("Ім'я має складатись тільки з літер. Введіть ім'я ще раз:");
    }
  
    newUser.lastName = prompt("Введіть прізвище:");
    while (!/^[a-zA-Z]*$/.test(newUser.lastName)) {
      newUser.lastName = prompt("Прізвище має складатись тільки з літер. Введіть прізвище ще раз:");
    }
  
    newUser.birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy:");
  
    // розрахунок віку користувача
    newUser.getAge = function() {
      const today = new Date();
      const birthDate = new Date(this.birthday);
      let age = today.getFullYear() - birthDate.getFullYear();
      const month = today.getMonth() - birthDate.getMonth();
      if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      
      return age;
    }
    
  
    // генерація пароля
    newUser.getPassword = function() {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const lastName = this.lastName.toLowerCase();
      const birthYear = this.birthday.split(".")[2];
      return firstLetter + lastName + birthYear;
    }
  
    console.log(`Створено нового користувача: ${newUser.firstName} ${newUser.lastName}`);
    console.log(`Вік користувача: ${newUser.getAge()} років`);
    console.log(`Згенерований пароль: ${newUser.getPassword()}`);
  
    return newUser;
  }
  console.log(createNewUser());
  console.log(getAge());
  console.log(getPassword());